# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# - Locate and install the TPythia6 External
# Defines:
#
#  TPYTHIA6_FOUND
#  TPYTHIA6_INCLUDE_DIR
#  TPYTHIA6_INCLUDE_DIRS (not cached)
#  TPYTHIA6_LIBRARIES
#
# Can be steered using TPYTHIA6_ROOT.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME TPythia6
   INCLUDE_SUFFIXES include INCLUDE_NAMES TPythia6/TPythia6.h
   LIBRARY_SUFFIXES lib
   COMPULSORY_COMPONENTS EGPythia6 )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( TPythia6 DEFAULT_MSG TPYTHIA6_INCLUDE_DIRS
   TPYTHIA6_LIBRARIES )
mark_as_advanced( TPYTHIA6_FOUND TPYTHIA6_INCLUDE_DIR TPYTHIA6_INCLUDE_DIRS
   TPYTHIA6_LIBRARIES TPYTHIA6_LIBRARY_DIRS )
